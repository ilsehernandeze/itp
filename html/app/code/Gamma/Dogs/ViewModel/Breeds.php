<?php


namespace Gamma\Dogs\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;

class Breeds implements ArgumentInterface
{
    public function woof(int $volume)
    {
        return $volume > 10 ? 'WOOF' : 'woof';
    }
}